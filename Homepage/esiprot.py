#!/usr/bin/python
# File: esiprot.py

from Tkinter import *
from tkMessageBox import *
from tkColorChooser import *
from math import *

# functions



def help_usage():
    showinfo('Help',"Read the README file \nJust try, it's easy!")

def help_about():
    showinfo('About',"ESIprot 1.0 \nLicense: GPLv3 \nRobert Winkler, 2009")

def calc_mw():
    mz=(mzvalue_1.get(),mzvalue_2.get(),mzvalue_3.get(),mzvalue_4.get(),mzvalue_5.get(),mzvalue_6.get(),mzvalue_7.get(),mzvalue_8.get(),mzvalue_9.get())
    mw=[mzvalue_1.get(),mzvalue_2.get(),mzvalue_3.get(),mzvalue_4.get(),mzvalue_5.get(),mzvalue_6.get(),mzvalue_7.get(),mzvalue_8.get(),mzvalue_9.get()]
    hydrogen=1.00794--0.0005485799
    # comment: atomic weight of hydrogen according to: Atomic weights of the elements 2005 (IUPAC Technical Report)
    # Reference: M. E. Wieser: Pure Appl. Chem., Vol. 78, No. 11. (2006), pp. 2051-2066.
    # The reported uncertainty of the atomic weight of hydrogen is +/- 0.00007 Da
    error=[0,0,0,0,0,0,0,0,0]
    charge=[0,0,0,0,0,0,0,0,0]
    stdev_champion=1000
    average_champion=1000000
    charge_champion=[0,0,0,0,0,0,0,0,0]
    mw_champion=[0,0,0,0,0,0,0,0,0]
    error_champion=[0,0,0,0,0,0,0,0,0]

    chargecount_1=charge_min.get()
    chargecount_2=charge_max.get()+1

    charge[0]=chargecount_1
    for i in range(1, 9, 1):
        charge[i]=charge[i-1]-1
    
    for j in range(chargecount_1,chargecount_2,1):
        sum=0
        nulls=0

        # calculation of molecular weigths and average molecular weight
        for i in range(0, 9, 1):
            mw[i]=(mz[i]*charge[i])-(charge[i]*hydrogen)
            if mz[i] == 0:
                mw[i] = 0
                nulls=nulls+1
            sum=sum+mw[i]
            charge[i]=charge[i]+1
        notnulls=9-nulls
        average=sum/(notnulls)

        # calculation of errros and standard deviation
        errorsquaresum=0
        for i in range(0, notnulls, 1):
            error[i]=mw[i]-average
            errorsquare=pow(error[i],2)
            errorsquaresum=errorsquaresum+errorsquare
            # comment: charge is incremented for next round, therefore must be decremented for reporting
            chargereport=charge[i]-1
            print 'm/z:', mz[i], 'charge:', chargereport,'+', 'MW [Da]:', mw[i], 'Error [Da]:', error[i]

        # calculation of standard deviation
        stdev=sqrt(errorsquaresum*pow((notnulls-1),-1))
        if stdev < stdev_champion:
            stdev_champion=stdev
            average_champion=average
            # fill in the values for the best approximation
            for z in range (0, 9, 1):
                chargevalue=charge[z]-1
                charge_champion[z]=chargevalue
                mw_champion[z]=mw[z]
                if mw_champion[z]== 0:
                    charge_champion[z]=0
                error_champion[z]=error[z]
            
        print 'Average MW [Da]:', average, 'Standard deviation [Da]:', stdev
        print '*******************************************************************************'
    print 'FINAL RESULTS'
    print '*******************************************************************************'
    for i in range(0, 9, 1):
         print 'm/z:', mz[i], 'charge:', charge_champion[i],'+', 'MW [Da]:', mw_champion[i], 'Error [Da]:', error_champion[i]
    print 'Deconvoluted MW [Da]:', average_champion, 'Standard deviation [Da]:', stdev_champion

    # output to GUI

    charge_value_1.set(charge_champion[0])
    charge_value_2.set(charge_champion[1])
    charge_value_3.set(charge_champion[2])
    charge_value_4.set(charge_champion[3])
    charge_value_5.set(charge_champion[4])
    charge_value_6.set(charge_champion[5])
    charge_value_7.set(charge_champion[6])
    charge_value_8.set(charge_champion[7])
    charge_value_9.set(charge_champion[8])
    mw_value_1.set(mw_champion[0])
    mw_value_2.set(mw_champion[1])
    mw_value_3.set(mw_champion[2])
    mw_value_4.set(mw_champion[3])
    mw_value_5.set(mw_champion[4])
    mw_value_6.set(mw_champion[5])
    mw_value_7.set(mw_champion[6])
    mw_value_8.set(mw_champion[7])
    mw_value_9.set(mw_champion[8])
    error_value_1.set(error_champion[0])
    error_value_2.set(error_champion[1])
    error_value_3.set(error_champion[2])
    error_value_4.set(error_champion[3])
    error_value_5.set(error_champion[4])
    error_value_6.set(error_champion[5])
    error_value_7.set(error_champion[6])
    error_value_8.set(error_champion[7])
    error_value_9.set(error_champion[8])
    average_value.set(average_champion)
    stdev_value.set(stdev_champion)

def clear_mzs():
    print 'clear m/z values'
    mzvalue_1.set(0)
    mzvalue_2.set(0)
    mzvalue_3.set(0)
    mzvalue_4.set(0)
    mzvalue_5.set(0)
    mzvalue_6.set(0)
    mzvalue_7.set(0)
    mzvalue_8.set(0)
    mzvalue_9.set(0)

# main window

root=Tk()
root.title("ESIprot 1.0")

# graphical user interface

features=Frame(root)
features.pack(side=LEFT)

# input of m/z values

header_label = Label(features, text="INPUT")
header_label.grid(row=1, column=2, sticky=W)

header_label = Label(features, text="Peaks from spectrum")
header_label.grid(row=2, column=2, sticky=W)

mz_label = Label(features, text="m/z (1):")
mz_label.grid(row=3, column=1, sticky=W)

mzvalue_1=DoubleVar()
mzvalue_1.set(1091.7)
mzvalue_entry = Entry(features, textvariable=mzvalue_1)
mzvalue_entry.grid(row=3, column=2, sticky=W)

mz_label = Label(features, text="m/z (2):")
mz_label.grid(row=4, column=1, sticky=W)

mzvalue_2=DoubleVar()
mzvalue_2.set(1182.5)
mzvalue_entry = Entry(features, textvariable=mzvalue_2)
mzvalue_entry.grid(row=4, column=2, sticky=W)

mz_label = Label(features, text="m/z (3):")
mz_label.grid(row=5, column=1, sticky=W)

mzvalue_3=DoubleVar()
mzvalue_3.set(1290.0)
mzvalue_entry = Entry(features, textvariable=mzvalue_3)
mzvalue_entry.grid(row=5, column=2, sticky=W)

mz_label = Label(features, text="m/z (4):")
mz_label.grid(row=6, column=1, sticky=W)

mzvalue_4=DoubleVar()
mzvalue_4.set(1418.8)
mzvalue_entry = Entry(features, textvariable=mzvalue_4)
mzvalue_entry.grid(row=6, column=2, sticky=W)

mz_label = Label(features, text="m/z (5):")
mz_label.grid(row=7, column=1, sticky=W)

mzvalue_5=DoubleVar()
mzvalue_5.set(1576.3)
mzvalue_entry = Entry(features, textvariable=mzvalue_5)
mzvalue_entry.grid(row=7, column=2, sticky=W)

mz_label = Label(features, text="m/z (6):")
mz_label.grid(row=8, column=1, sticky=W)

mzvalue_6=DoubleVar()
mzvalue_6.set(1773.3)
mzvalue_entry = Entry(features, textvariable=mzvalue_6)
mzvalue_entry.grid(row=8, column=2, sticky=W)

mz_label = Label(features, text="m/z (7):")
mz_label.grid(row=9, column=1, sticky=W)

mzvalue_7=DoubleVar()
mzvalue_7.set(0)
mzvalue_entry = Entry(features, textvariable=mzvalue_7)
mzvalue_entry.grid(row=9, column=2, sticky=W)

mz_label = Label(features, text="m/z (8):")
mz_label.grid(row=10, column=1, sticky=W)

mzvalue_8=DoubleVar()
mzvalue_8.set(0)
mzvalue_entry = Entry(features, textvariable=mzvalue_8)
mzvalue_entry.grid(row=10, column=2, sticky=W)

mz_label = Label(features, text="m/z (9):")
mz_label.grid(row=11, column=1, sticky=W)

mzvalue_9=DoubleVar()
mzvalue_9.set(0)
mzvalue_entry = Entry(features, textvariable=mzvalue_9)
mzvalue_entry.grid(row=11, column=2, sticky=W)

# input of charges

charge_label = Label(features, text="charge min. (+):")
charge_label.grid(row=15, column=1, sticky=W)

charge_min=IntVar()
charge_min.set(1)
charge_entry = Entry(features, textvariable=charge_min)
charge_entry.grid(row=15, column=2, sticky=W)

charge_label = Label(features, text="charge max. (+):")
charge_label.grid(row=16, column=1, sticky=W)

charge_max=IntVar()
charge_max.set(100)
charge_entry = Entry(features, textvariable=charge_max)
charge_entry.grid(row=16, column=2, sticky=W)

# output of results

header_label = Label(features, text="RESULTS")
header_label.grid(row=1, column=4, sticky=W)

# charge results

header_label = Label(features, text="charge (+)")
header_label.grid(row=2, column=4, sticky=W)

charge_value_1=IntVar()
charge_value_1.set(0)
charge_result = Entry(features, textvariable=charge_value_1)
charge_result.grid(row=3, column=4, sticky=W)

charge_value_2=IntVar()
charge_value_2.set(0)
charge_result = Entry(features, textvariable=charge_value_2)
charge_result.grid(row=4, column=4, sticky=W)

charge_value_3=IntVar()
charge_value_3.set(0)
charge_result = Entry(features, textvariable=charge_value_3)
charge_result.grid(row=5, column=4, sticky=W)

charge_value_4=IntVar()
charge_value_4.set(0)
charge_result = Entry(features, textvariable=charge_value_4)
charge_result.grid(row=6, column=4, sticky=W)

charge_value_5=IntVar()
charge_value_5.set(0)
charge_result = Entry(features, textvariable=charge_value_5)
charge_result.grid(row=7, column=4, sticky=W)

charge_value_6=IntVar()
charge_value_6.set(0)
charge_result = Entry(features, textvariable=charge_value_6)
charge_result.grid(row=8, column=4, sticky=W)

charge_value_7=IntVar()
charge_value_7.set(0)
charge_result = Entry(features, textvariable=charge_value_7)
charge_result.grid(row=9, column=4, sticky=W)

charge_value_8=IntVar()
charge_value_8.set(0)
charge_result = Entry(features, textvariable=charge_value_8)
charge_result.grid(row=10, column=4, sticky=W)

charge_value_9=IntVar()
charge_value_9.set(0)
charge_result = Entry(features, textvariable=charge_value_9)
charge_result.grid(row=11, column=4, sticky=W)


#  molecular weight results

header_label = Label(features, text="MW [Da]")
header_label.grid(row=2, column=5, sticky=W)

mw_value_1=DoubleVar()
mw_value_1.set(0)
mw_result = Entry(features, textvariable=mw_value_1)
mw_result.grid(row=3, column=5, sticky=W)

mw_value_2=DoubleVar()
mw_value_2.set(0)
mw_result = Entry(features, textvariable=mw_value_2)
mw_result.grid(row=4, column=5, sticky=W)

mw_value_3=DoubleVar()
mw_value_3.set(0)
mw_result = Entry(features, textvariable=mw_value_3)
mw_result.grid(row=5, column=5, sticky=W)

mw_value_4=DoubleVar()
mw_value_4.set(0)
mw_result = Entry(features, textvariable=mw_value_4)
mw_result.grid(row=6, column=5, sticky=W)

mw_value_5=DoubleVar()
mw_value_5.set(0)
mw_result = Entry(features, textvariable=mw_value_5)
mw_result.grid(row=7, column=5, sticky=W)

mw_value_6=DoubleVar()
mw_value_6.set(0)
mw_result = Entry(features, textvariable=mw_value_6)
mw_result.grid(row=8, column=5, sticky=W)

mw_value_7=DoubleVar()
mw_value_7.set(0)
mw_result = Entry(features, textvariable=mw_value_7)
mw_result.grid(row=9, column=5, sticky=W)

mw_value_8=DoubleVar()
mw_value_8.set(0)
mw_result = Entry(features, textvariable=mw_value_8)
mw_result.grid(row=10, column=5, sticky=W)

mw_value_9=DoubleVar()
mw_value_9.set(0)
mw_result = Entry(features, textvariable=mw_value_9)
mw_result.grid(row=11, column=5, sticky=W)

#  error results

header_label = Label(features, text="error [Da]")
header_label.grid(row=2, column=6, sticky=W)

error_value_1=DoubleVar()
error_value_1.set(0)
error_result = Entry(features, textvariable=error_value_1)
error_result.grid(row=3, column=6, sticky=W)

error_value_2=DoubleVar()
error_value_2.set(0)
error_result = Entry(features, textvariable=error_value_2)
error_result.grid(row=4, column=6, sticky=W)

error_value_3=DoubleVar()
error_value_3.set(0)
error_result = Entry(features, textvariable=error_value_3)
error_result.grid(row=5, column=6, sticky=W)

error_value_4=DoubleVar()
error_value_4.set(0)
error_result = Entry(features, textvariable=error_value_4)
error_result.grid(row=6, column=6, sticky=W)

error_value_5=DoubleVar()
error_value_5.set(0)
error_result = Entry(features, textvariable=error_value_5)
error_result.grid(row=7, column=6, sticky=W)

error_value_6=DoubleVar()
error_value_6.set(0)
error_result = Entry(features, textvariable=error_value_6)
error_result.grid(row=8, column=6, sticky=W)

error_value_7=DoubleVar()
error_value_7.set(0)
error_result = Entry(features, textvariable=error_value_7)
error_result.grid(row=9, column=6, sticky=W)

error_value_8=DoubleVar()
error_value_8.set(0)
error_result = Entry(features, textvariable=error_value_8)
error_result.grid(row=10, column=6, sticky=W)

error_value_9=DoubleVar()
error_value_9.set(0)
error_result = Entry(features, textvariable=error_value_9)
error_result.grid(row=11, column=6, sticky=W)

# some extra space

header_label = Label(features, text="   ")
header_label.grid(row=2, column=7, sticky=W)

header_label = Label(features, text="   ")
header_label.grid(row=12, column=2, sticky=W)

header_label = Label(features, text="   ")
header_label.grid(row=14, column=2, sticky=W)

header_label = Label(features, text="   ")
header_label.grid(row=17, column=2, sticky=W)

header_label = Label(features, text="   ")
header_label.grid(row=17, column=3, sticky=W)

# deconvoluted MW and standard deviation

header_label = Label(features, text="Deconvoluted MW [Da]:")
header_label.grid(row=15, column=5, sticky=W)

average_value=DoubleVar()
average_value.set(0)
average_result = Entry(features, textvariable=average_value)
average_result.grid(row=15, column=6, sticky=W)

header_label = Label(features, text="Std. deviation [Da]:")
header_label.grid(row=16, column=5, sticky=W)

stdev_value=IntVar()
stdev_value.set(0)
stdev_result = Entry(features, textvariable=stdev_value)
stdev_result.grid(row=16, column=6, sticky=W)

# action buttons

action_button=Button(features, text="Calculate MW", command=calc_mw)
action_button.grid(row=13, column=4)

action_button=Button(features, text="Clear m/z values", command=clear_mzs)
action_button.grid(row=13, column=2)

# create a menu

menu = Menu(root)
root.config(menu=menu)

filemenu = Menu(menu)
menu.add_cascade(label="File", menu=filemenu)
filemenu.add_command(label="Exit", command=menu.quit)

helpmenu = Menu(menu)
menu.add_cascade(label="Help", menu=helpmenu)
helpmenu.add_command(label="Usage", command=help_usage)
helpmenu.add_command(label="About...", command=help_about)

mainloop()



