<html>
<head>
<title>ESIprot Online</title>
</head>
<body>
<h1>ESIprot Online</h1>
<?php

$mz=array(0,0,0,0,0,0,0,0,0);
$mz[0]=$_POST[mz1];
$mz[1]=$_POST[mz2];
$mz[2]=$_POST[mz3];
$mz[3]=$_POST[mz4];
$mz[4]=$_POST[mz5];
$mz[5]=$_POST[mz6];
$mz[6]=$_POST[mz7];
$mz[7]=$_POST[mz8];
$mz[8]=$_POST[mz9];

$mw=$mz;
$hydrogen=1.00794-0.0005485799;

$error=array(0,0,0,0,0,0,0,0,0);
$charge=array(0,0,0,0,0,0,0,0,0);
$stdev_champion=1000;
$average_champion=1000000;
$charge_champion=array(0,0,0,0,0,0,0,0,0);
$mw_champion=array(0,0,0,0,0,0,0,0,0);
$error_champion=array(0,0,0,0,0,0,0,0,0);

$chargecount_1=$_POST[min_charge];
if ($chargecount_1 <1)
{
$chargecount_1=1;
}

$chargecount_2=$_POST[max_charge];
if ($chargecount_2 <2)
{
$chargecount_2=100;
}

$charge[0]=$chargecount_1;

for ($i=1; $i<=9; $i++)
{
        $charge[$i]=$charge[$i-1]-1;
}

for ($j=$chargecount_1; $j<=$chargecount_2; $j++)
{
        $sum=0;
		$nulls=0;
		
		/*calculation of molecular weigths and average molecular weight*/
        for ($i=0; $i<=8; $i++)
        {
			$mw[$i]=($mz[$i]*$charge[$i])-($charge[$i]*$hydrogen);
            if ($mz[$i] == 0)
            {
				$mw[$i] = 0;
                $nulls=$nulls+1;
            }
			$sum=$sum+$mw[$i];
            $charge[$i]=$charge[$i]+1;
        }
		$notnulls=9-$nulls;
        $average=$sum/$notnulls;
		
		/*calculation of errros and standard deviation*/
		$errorsquaresum=0;
		for ($i=0; $i<=$notnulls-1; $i++)
        {
			$error[$i]=$mw[$i]-$average;
            $errorsquare=pow($error[$i],2);
            $errorsquaresum=$errorsquaresum+$errorsquare;
            /* comment: charge is incremented for next round, therefore must be decremented for reporting*/
            $chargereport=$charge[$i]-1;
		}
        /*calculation of standard deviation*/
        $stdev=sqrt($errorsquaresum*pow(($notnulls-1),-1));
        if ($stdev < $stdev_champion)
		{
            $stdev_champion=$stdev;
            $average_champion=$average;
            /*fill in the values for the best approximation*/
			for ($z=0; $z<=8; $z++)
            {
                $chargevalue=$charge[$z]-1;
                $charge_champion[$z]=$chargevalue;
                $mw_champion[$z]=$mw[$z];
                if ($mw_champion[$z]== 0)
				{
				$charge_champion[$z]=0;
                }
				$error_champion[$z]=$error[$z];
			}
        }   
}

    echo '<h1>ESIprot Online calculation </h1>';
	echo '<b>INPUT  </b><br>';
	echo 'Charge min.: ' . $chargecount_1 . "+<br>";
	echo 'Charge max.: ' . $chargecount_2 . "+<br>";
	echo 'ESI positive mode, H+ ions <br><br>';
	
	echo "<table border>";
	echo "<tr><td><b>m/z</b></td><td><b>charge</b></td><td><b>MW [Da]</b></td><td><b>Error [Da]</b></td></tr>";
    for ($i=0; $i<=8; $i++)
	{
		if ($mz[$i] > 0)
		{
         echo "<tr><td>" . $mz[$i] . "</td><td>" . $charge_champion[$i] . "+ </td><td>" . $mw_champion[$i] . "</td><td>" . $error_champion[$i] . "</td></tr>";
		 }
    }
	echo "</table><br>";
	echo '<b>Deconvoluted MW [Da]: ' . $average_champion . "</b><br>"; 
	echo '<b>Standard deviation [Da]: ' . $stdev_champion . "</b><br>";

echo '</b><br><a href=http://www.bioprocess.org/esiprot/esiprot_form.php>New ESIprot Online calculation</a></b><br>';
	
echo '<h3>Please cite:</h3> <b>Winkler R.</b>: ESIprot: A universal tool for charge state determination and molecular weight calculation of proteins from electrospray ionization mass spectrometry data. <i>Rapid Commun Mass Spectrom</i>, 24(3),  285-294, 2010, '; 
echo '<a href=http://dx.doi.org/10.1002/rcm.4384>Full text on RCM</a><br><br>';
  
    /* comment: atomic weight of hydrogen according to: Atomic weights of the elements 2005 (IUPAC Technical Report)
    Reference: M. E. Wieser: Pure Appl. Chem., Vol. 78, No. 11. (2006), pp. 2051-2066.
    The reported uncertainty of the atomic weight of hydrogen is +/- 0.00007 Da*/
	
?>

</body>
</html>
