<html>
<head>
<title>ESIprot Online</title>
</head>
<body>
<h1>ESIprot Online</h1>
ESIprot Online enables the charge state determination and molecular weight calculation for low resolution electrospray ionization (ESI) mass spectrometry (MS) data of proteins.<br>
This is the PHP based online version of <a href="http://www.lababi.bioprocess.org/index.php/lababi-software/84-esiprot">ESIprot</a>.
<?php

echo "<h2>Input m/z values</h2>";
echo "<form action='esiprot_calculation.php' method='post'>";
echo "<table >";
echo "<tr><td>m/z 1 (e.g. 1019.0):</td><td><input name='mz1'></td></tr>";
echo "<tr><td>m/z 2 (e.g. 1061.4):</td><td><input name='mz2'></td></tr>";
echo "<tr><td>m/z 3 (e.g. 1107.5):</td><td><input name='mz3'></td></tr>";
echo "<tr><td>m/z 4 (e.g. 1157.8):</td><td><input name='mz4'></td></tr>";
echo "<tr><td>m/z 5 (e.g. 1212.9):</td><td><input name='mz5'></td></tr>";
echo "<tr><td>m/z 6 (e.g. 1273.5):</td><td><input name='mz6'></td></tr>";
echo "<tr><td>m/z 7 (e.g. 1340.4):</td><td><input name='mz7'></td></tr>";
echo "<tr><td>m/z 8 (e.g. 1414.8):</td><td><input name='mz8'></td></tr>";
echo "<tr><td>m/z 9 (e.g. 1498.0):</td><td><input name='mz9'></td></tr>";
echo "<tr><td>min charge (default: 1):</td><td><input name='min_charge'></td></tr>";
echo "<tr><td>max charge (default: 100):</td><td><input name='max_charge'></td></tr>";
echo "</table><br>";
echo "At least <b>m/z 1</b> and <b>m/z 2</b> must be provided<br>";

echo "<br>";

echo "	<input type='reset' value='Clear m/z values'>";
echo "	<input type='submit' value='Calculate MW'>";

echo "</form>";
echo "* the example m/z values correspond to the active form of alpha-chymotrypsin from bovine pancreas, with five disulfide (S-S) bonds   and attachment of TN[147-148] (reported in RCM, 2010).";
?>
<h2>Background</h2>
Electrospray ionization (ESI) mass spectrometry (MS) devices with relatively low resolution are widely used for proteomics and metabolomics. Ion trap devices like the Agilent MSD/XCT ultra or the Bruker HCT ultra are typical representatives. However, even if ESI-MS data of most of the naturally ocurring proteins can be measured, the availability of data evaluation software for such ESI protein spectra with low resolution is quite limited.

<br />

<img src="fig3_alac_charge_MW.png" ALIGN=RIGHT WIDTH=40% BORDER=0 alt="Deconvoluted ESI mass spectrum">

<br />

<h2>Main characteristics of ESIprot</h2>   
<ul>
    <li>Algorithm based on standard deviation optimization (scatter minimization)</li>
    <li>Suitable for peak lists. No raw data and peak intesities required, therefore suitable to re-evaluate "historical" data</li>
    <li>Highly accurate calculation of molecular weights of proteins, even from low resolution data</li>
    <li>Only two peaks of one protein required, extremly robust</li>
    <li>Theoretically unlimited molecular weight range</li>
    <li>Designed for the analysis of pure proteins</li>
    <li>Open source license GPL v3: program and parts of it may be used for commercial and non-commerical purposes</li>
</ul>

<h2>Reference</h2>
<b>Winkler R.</b>: ESIprot: A universal tool for charge state determination and molecular weight calculation of proteins from electrospray ionization mass spectrometry data. <i>Rapid Commun Mass Spectrom</i>, 24(3),  285-294, 2010, <a href=http://dx.doi.org/10.1002/rcm.4384>Full text on RCM</a><br><br>

The Microsoft Windows version of ESIprot, as well as the Python source code (License GPL v3) are available at the
<a href="http://www.lababi.bioprocess.org/index.php/lababi-software/84-esiprot">ESIprot webpage</a>.

</body>
</html>
