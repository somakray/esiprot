# README #

ESIprot Online enables the charge state determination and molecular weight calculation for low resolution electrospray ionization (ESI) mass spectrometry (MS) data of proteins. Bitbucket contains the source code of the algorithm. If you just want to use ESIprot, you can use the online version at: http://www.bioprocess.org/esiprot/esiprot_form.php

### Contribution ###

Please notify me on any error you might encounter.
Thanks to Dr. Chris Rath for his comments on the electron mass difference. The electron mass is considered in the current ESIprot version 1.1. 

### Contact ###

robert.winkler@bioprocess.org; robert.winkler@cinvestav.mx